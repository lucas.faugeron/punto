import React from 'react';

const Card = ({color, number}) => {
    return (
        <div className="card">
            <p className={color}>{number}</p>
        </div>
    );
};

export default Card;