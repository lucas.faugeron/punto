import Card from "./components/Card";

function App() {
  return (
    <div className="App">
        HELLO REACT
        <br/>

        <Card color="red" number="3" />
        <Card color="blue" number="9" />
    </div>
  );
}

export default App;
